![Build Status](https://gitlab.com/pages/harp/badges/master/build.svg)

---

Harp website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

**Table of Contents**

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: node:4.2.2

pages:
  cache:
    paths:
    - vendor

  script:
  - npm install -g harp
  - harp compile ./ public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Harp
1. Generate the website: `harp compile ./ public`
1. Preview your project: `harp server --port 9000`
1. Add content

Read more at Harp's [documentation][].

Forked from @VeraKolotyuk

[ci]: https://about.gitlab.com/gitlab-ci/
[harp]: http://harpjs.com/
[install]: http://harpjs.com/docs/quick-start
[documentation]: http://harpjs.com/docs/
